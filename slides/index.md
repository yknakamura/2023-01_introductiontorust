---
title: シン・ネイティブ言語 〜 Rust
marp: true
paginate: true
markdown.marp.enableHtml: true
style: |
  section.primary_section {
    background-image: url("images/rustacean-orig-noshadow.svg");
    background-size: 30%;
    background-position: right center;
    background-repeat: no-repeat;
  }

  section.primary_section h1 {
    font-size: 2.4rem;
    text-shadow: #FFF 0px 0 10px;
  }

  section.primary_section .author {
    font-size: 0.8rem;
    color: #999;
  }

  section.primary_section ul {
    font-size: 0.8rem;
    padding: 0;
  }

  section.primary_section li {
    list-style-type: none;
  }

  section::after {
    content: attr(data-marpit-pagination) ' / ' attr(data-marpit-pagination-total);
  }

  .columns {
    display: grid;
    grid-template-columns: repeat(2, minmax(0, 1fr));
    gap: 1rem;
  }
---

<!-- _class: primary_section -->

# シン・ネイティブ言語 〜 Rust

<div class="author">
2023-01 by Yuki Nakamura
</div>

![Slides are here](images/qrcode.png)

- スライド: https://yknakamura.gitlab.io/2023-01_introductiontorust
- リポジトリ: https://gitlab.com/yknakamura/2023-01_introductiontorust

---

## 自己紹介

- 名前: Yuki Nakamura
- Rust歴: 数年前からお勉強中
- 守備範囲: 主にバックエンド（APIなど）
    - <small>最近はフロントエンド（React）もチョトデキル</small>

---

## 今日話す内容

- Rustについて
  - Rustの特徴と思想
  - 言語仕様としての特徴
  - 利用分野や活用事例
- Rustのはじめかた
  - 公式サイトやドキュメントの紹介
  - Playground
  - 開発環境の準備
  - プロジェクト構成

ボリュームが多くて大変ですが、後からじっくり見返せる資料にもなるようにリンクを沢山入れてあります。

---

## 今日ターゲットにする人

- プログラマ、コードを書くエンジニア
- 新しいモダンなプログラミング言語を学んでみたい人
- 何かと耳にするので気になっている人
- C言語よりもメモリ安全な言語が欲しい人
- クロスプラットフォームなツールを作りたい人
- 幅広い分野で使える言語を習得したい人

**まずはRustがどんなものなのかを知ってもらおうと思います。**
**細かい文法や実装方法などの解説は行いません。**
<small>（言語の特徴や雰囲気を知ってもらうためのサンプルコードは出てきます。）</small>

---

# Rustの概要

---

## Rustとは

- [公式サイト](https://www.rust-lang.org/) → https://www.rust-lang.org
- オープンソース
- マルチパラダイム言語
- C、C++の代わりになる言語を目指している
- [Stack Overflow Developer Survey](https://insights.stackoverflow.com/survey/)の[「最も愛されているプログラミング言語」](https://survey.stackoverflow.co/2022/#most-loved-dreaded-and-wanted-language-love-dread)で7年間一位を獲得し続けている

---

## ロゴとマスコットキャラクター

<div class="columns">
<div>

### ロゴ

![Rust logo w:300px](images/rust-logo-512x512-blk.png)

※自転車のチェーンリングがモチーフ

</div>

<div>

### マスコットキャラクター Ferris

![Ferris w:400px](images/rustacean-orig-noshadow.svg)

※英語で甲殻類をc**rust**aceanという
<small>※非公式だが、[公式ドキュメントでも使われている](https://doc.rust-lang.org/book/ch00-00-introduction.html#ferris)（半ば公認？）</small>

</div>
</div>

---

## Rustの歴史

- 2006年頃、Mozillaで働くGraydon Hoareさんが個人的に作り始めた
- 2009年、Mozillaがチームを編成して本格的に開発、2010年にMozilla Summit 2010で公開された
- 2011年、Rustコンパイラのセルフホスティングが実現された（それまではOCamlで書かれていた）
- 2015年、最初の安定版であるRust 1.0がリリースされた
- 2016年、Firefoxの一部のコードがRustで書き直された
- 2018年、エディション制が導入される（`Rust 2015`、`Rust 2018`など）
  - 互換性のない変更は新しいエディションにのみ適用される
  - メジャーバージョンとは少し違って、設定でどのエディションを使用するか選択できる  

---

## Rustの特徴

- 性能、メモリ安全性、安全な並列性を目指して設計された言語
- C、C++に代わるプログラミング言語を目指す
- 静的型付け
- 所有権と借用によるリソース管理
- クロスコンパイル
- WebAssembly(WASM)対応
- 今どきの開発ツールが最初から揃っている
  - ツールチェーン管理、パッケージマネージャー兼ビルド管理、LSP、フォーマッター、lint、ドキュメントツール、ユニットテスト

---

### Rustの特徴（その２）

- 実装上の選択肢が多い
  - 組み込みやOSレベルから、Webアプリまで幅広いレイヤーをカバーするため
  - 並列処理も様々な方式のライブラリが存在する
    - 非同期処理は文法のみが規定されていて、実装はライブラリで提供される
- 学習難易度が高い
  - 他の言語にはあまり見られない難しい概念（所有権、ライフタイムなど）
  - 作法が厳しい
- **とにかくコンパイルが通らない！！！**

---

## Rustのコードサンプル

```rust
fn main() {
    let user = "Alice";

    greeting(user);
}

fn greeting(name: &str) {
    println!("Hello, {}!", name);
}
```

<small>コード: https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=750ff2cd7dbbd5189fccfce7621e4bb6</small>

---

## Rustの目標・設計思想

- 性能、[メモリ安全性][^memory-safety]、安全な並列性を目指して設計された言語
- C、C++に代わるプログラミング言語を目指す

[^memory-safety]: https://ja.wikipedia.org/wiki/%E3%83%A1%E3%83%A2%E3%83%AA%E5%AE%89%E5%85%A8%E6%80%A7

### C、C++は…

- 性能はトップクラス
- メモリ安全ではない言語
  - メモリを直接操作できる（高い自由度と柔軟性）
  - メモリの管理はプログラマに委ねられる（言語やランタイムによる保護機構がない）
    - `malloc/free`、`new/delete`をプログラマが管理する必要がある

---

## メモリ安全性を重視する時代

- [グーグル、Rust採用で「Android」のメモリーに関わる脆弱性が激減 - ZDNET Japan](https://japan.zdnet.com/article/35196972/)
- [米国家安全保障局、CやC＋＋からメモリ安全なプログラミング言語への移行を推奨する文書を公開|CodeZine（コードジン）](https://codezine.jp/article/detail/16854)
- [疑われる「C++」の安全性、今後の動きはどうなる - ZDNET Japan](https://japan.zdnet.com/article/35199018/)
- メモリ関連のバグはセキュリティ事故に繋がるような深刻なバグが多い
- CやC++のようなメモリ安全でない言語からメモリ安全な言語に切り替えよう、という動き

---

## Rustの性能に寄与する仕組み

- 機械語にコンパイルされる（[LLVM](https://ja.wikipedia.org/wiki/LLVM)を利用）
- [ゼロコスト抽象化](https://tomoyuki-nakabayashi.github.io/book/static-guarantees/zero-cost-abstractions.html)
  - 抽象化の仕組みが実行時のオーバーヘッドなしに動作する
  - コンパイル時に解決する
  - 参考: [Rustのゼロコスト抽象化の効果をアセンブラで確認](https://blog.rust-jp.rs/tatsuya6502/posts/2019-12-zero-cost-abstraction/)
- ガベージコレクションを利用しない

---

## Rustの安全性に寄与する仕組み

- 型安全性
  - 型検査が通ったなら、実行時には型に起因するバグは起きない
- メモリ管理
  - 不正なメモリ操作を許さない
    - 未初期化／解放済みの変数にアクセス、二重解放、範囲外へのアクセス、etc...
  - ポインタを使って値に直接アクセスできない
  - 所有権とライフタイムによるメモリ管理
- コンパイル時チェック
  - 多くのチェックがコンパイル時に行われる

**ルールから外れたコードはコンパイルエラー**

参考: [安全と危険のご紹介](https://doc.rust-jp.rs/rust-nomicon-ja/meet-safe-and-unsafe.html) 

---

## 実は安全ではないコードも書ける

- 生のポインタを扱う
- Cなどで書かれたメモリ安全ではない関数を呼び出す
- [static変数へのmutableなアクセス](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=f23778906da75333edb276bc4ddcefae)

参考: [安全と危険のご紹介](https://doc.rust-jp.rs/rust-nomicon-ja/meet-safe-and-unsafe.html) 

---

## なぜ安全ではないコードも書けるのか

- Cなど他の言語と連携できるようにするため（ライブラリなど）
- 低レイヤープログラミングを可能にするため
  - OSやハードウェアを直接操作するなど
- 安全性を犠牲にしてでもパフォーマンスを向上させるため

とはいえ…

- 安全ではないコードを書くにもルールがある
- 安全ではないコードの範囲は限定的になる
  - 問題が発生しても特定しやすい

参考: [Unsafe Rust - The Rust Programming Language 日本語版](https://doc.rust-jp.rs/book-ja/ch19-01-unsafe-rust.html)

---

## unsafeの例

```rust
extern "C" {
    fn abs(input: i32) -> i32;
}

fn main() {
    unsafe {
        // -3の絶対値は、Cによると{}
        println!("Absolute value of -3 according to C: {}", abs(-3));
    }
}
```

<small>引用元: [Unsafe Rust - The Rust Programming Language 日本語版](https://doc.rust-jp.rs/book-ja/ch19-01-unsafe-rust.html)</small>

**`unsafe`** がキーワード

---

# Rustのプログラミング言語としての特徴

言語仕様などの話

---

## Rustにないもの

- クラス、オブジェクト
- プロトタイプ
- 継承
- オーバーロード
- 例外

いわゆるオブジェクト指向言語らしい要素は持っていない。

---

## Rustにあるもの

- [構造体](https://doc.rust-jp.rs/book-ja/ch05-00-structs.html)
- [トレイト](https://doc.rust-jp.rs/book-ja/ch10-02-traits.html)<small>（trait = 「特性」「特徴」といった意味）</small>
  - 他の言語でいうインターフェイスのようなもの
- [ジェネリクス](https://doc.rust-jp.rs/book-ja/ch10-01-syntax.html)（総称型）
- [所有権](https://doc.rust-jp.rs/book-ja/ch04-01-what-is-ownership.html)（と借用）と[ライフタイム](https://doc.rust-jp.rs/book-ja/ch10-03-lifetime-syntax.html)
- [モジュール](https://doc.rust-jp.rs/book-ja/ch07-02-defining-modules-to-control-scope-and-privacy.html)
- [マクロ](https://doc.rust-jp.rs/book-ja/ch19-06-macros.html)
- [アトリビュート](https://doc.rust-jp.rs/rust-by-example-ja/attribute.html)

---

## オブジェクト指向プログラミングについて

- クラスや継承がないということは、Rustはオブジェクト指向ではない？
  - →YesともNoとも言える
- 他の道具を使って、オブジェクト指向と似たようなプログラミングができる
  - クラス、オブジェクト → **構造体、impl**
  - カプセル化 → **モジュール単位の可視性**
  - 継承 → 委譲、**トレイト境界の実装、トレイト継承**
  - ポリモーフィズム → **ジェネリクス、トレイトオブジェクト**

近年、継承をベースとしたオブジェクト指向はアンチパターンと考えられており、最近の言語では避ける傾向にある。

---

## Rustとオブジェクト指向についての参考記事

- [オブジェクト指向言語の特徴 - The Rust Programming Language 日本語版](https://doc.rust-jp.rs/book-ja/ch17-01-what-is-oo.html)
- [Rustを通して見るオブジェクト指向｜TechRacho by BPS株式会社](https://techracho.bpsinc.jp/yoshi/2022_08_29/121213)
- [Rustがオブジェクト指向型言語ではないのとその理由 - なんか考えてることとか](https://opaupafz2.hatenablog.com/entry/2021/06/12/104719)
- [オブジェクト指向経験者のためのRust入門 - Qiita](https://qiita.com/nacika_ins/items/cf3782bd371da79def74#rust%E3%81%AB%E3%81%AFclass%E3%81%8C%E3%81%AA%E3%81%84)

---

# Rustの言語としての主な特徴

他の言語とは異なる大きな特徴

---

## Rustの主な特徴をピックアップして紹介

- **構造体とトレイト**
- 所有権と借用
- ライフタイム

他にも様々な要素がありますが、Rustの目玉に絞って紹介します。

---

## 構造体とトレイト

- 他の言語の構造体、インターフェイスとだいたい同じ
  - 構造体: レコード型（名前付きの複数のフィールドで構成される型）
  - トレイト: インターフェイス
- 構造体は`struct`、トレイトは`trait`キーワードで定義
- `impl`を使って**構造体に対してメソッドや関連関数を定義できる**
  - メソッド: 構造体のインスタンスを対象とした関数
  - 関連関数: インスタンスは取らない（コンストラクタなどに使われる）

概念としては異なるけど、実質的にクラスみたいな感じで扱える。

---

## 構造体とトレイト（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=10d7c1639692d5205b0e99c7aa0a4619)）

```rust
trait Shape {               // トレイトの定義
    fn area(&self) -> f64;
}

struct Rectangle {          // 構造体の定義
    width: f64,
    height: f64,
}

impl Shape for Rectangle {  // RectangleにShapeトレイトを実装
    fn area(&self) -> f64 {
        self.width * self.height
    }
}

struct Circle {             // 別の構造体
    radius: f64,
}

impl Shape for Circle {     // CircleにもShapeトレイトを実装
    fn area(&self) -> f64 {
        3.14 *  self.radius * self.radius
    }
}

fn print_area(name: &str, shape: &impl Shape) {  // Shapeトレイトを実装した型を受け取る
    println!("{}の面積: {}", name, shape.area())  // Shapeトレイトのarea()を呼び出す
}
```

---

## Rustの主な特徴をピックアップして紹介

- 構造体とトレイト
- **所有権と借用**
- ライフタイム

---

## 所有権(Ownership)

所有権はRustの特徴としてよく挙げられるもののひとつ。

- ガベージコレクションを使わずに安全にメモリを管理するための仕組み
- 所有権: 変数が値を保持できる権利（あるいは破棄できる権利）
- 所有者: 所有権を持った変数
  - いかなる時も所有者は1つ
  - 所有者（変数）がスコープから外れると値は破棄される
- 別の変数に代入したり、関数に渡したりすると所有権が移動（ムーブ）する
- 所有権を渡さずに参照させる**借用**(borrowing)という機能もある

---

## 所有権（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=6c59e2fff429a9d82eee098fa5a019fe)、[関数の例](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=d3b46113e3a4900177efea2df1ebf4ab)）

```rust
#[allow(unused_variables)]
fn main() {
    let s1 = String::from("Hello"); // String型の値をs1に束縛
    let s2 = s1;                    // s1からs2へ所有権が移動

    println!("{}", s1);             // s1は所有権を失っているので使えない！
} // ここでスコープを抜けて、s2のメモリを解放する（s1は所有権を失っているので何もしない）
```

```
error[E0382]: borrow of moved value: `s1`
 --> src/main.rs:6:20
  |
3 |     let s1 = String::from("Hello");
  |         -- move occurs because `s1` has type `String`, which does not implement the `Copy` trait
4 |     let s2 = s1;
  |              -- value moved here
5 |
6 |     println!("{}", s1);
  |                    ^^ value borrowed here after move
  |
help: consider cloning the value if the performance cost is acceptable
  |
4 |     let s2 = s1.clone();
  |                ++++++++
```

---

## 参照と借用（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=c1253224b1a91dd016df3bdc9e56bf1e)）

```rust
fn print_value(value: &String) { // 引数に参照を取る（借用と呼ぶ）
    println!("{}", value);
} // valueは借用であり、所有権を持っているわけではないので何もしない

fn main() {
    let s = String::from("Hello");  // sがスコープに入る

    print_value(&s);    // sの参照を渡す（所有権は渡さない）

    println!("{}", s);  // sの所有権を持っているので使える
}
```

- `参照(&)`は所有権を持たない
- 関数の引数に参照を取ることを`借用`と呼ぶ

---

## 所有権（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=9115098325bdf5d9f027be58935f4c6a)）

ところで、次のコードは前述の例を整数に変えただけだが正常に動く。

```rust
#[allow(unused_variables)]
fn main() {
    let a = 100;
    let b = a;    // ここでコピーされる（aは所有権を失わない）

    println!("{}", a);
}
```

- `i32`のように`Copy`トレイトが実装されている型はコピーされる（**コピーセマンティクス**になる）
  - それ以外の場合は所有権が移動する（**ムーブセマンティクス**）

---

## 所有権って難しくないですか！？

- 難しいです
- 「Rustは難しい」と言われる要素のひとつ
- なんかわかんないけどコンパイルが通らなくて躓く人多数

---

## 所有権がもたらすメリット

- メモリの二重開放を防止できる
  - 所有者がメモリを解放する
  - 所有権を持っていないものは何もしない
- メモリリークを防止できる
  - スコープを抜ける時にメモリを解放する
- GCのような実行時のメモリ管理コストが必要ない

---

## Rustの主な特徴をピックアップして紹介

- 構造体とトレイト
- 所有権と借用
- **ライフタイム**

---

## ライフタイム

所有権と並び、Rustの特徴としてよく挙げられるもののひとつ。

- 参照が有効な期間のこと
- ライフタイムによって借用に問題がないことをチェックする
- ライフタイム注釈
  - `'a`のようにアポストロフィーに続く名前で表現する
    例: `&'a i32`
- 大体の場合、ライフタイムは暗黙的に推論される（わざわざ注釈を書かなくてもよい）
- プログラムの実行中に常に存在していることを示す`'static`もある（文字列リテラルなど）

---

## ライフタイム（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=90e61012a77096721e25edb6e58b6261)）

```rust
fn main() {
    {
        let x = 5;            // ----------+-- 'b
                              //           |
        let r = &x;           // --+-- 'a  |
                              //   |       |
        println!("r: {}", r); //   |       |
                              // --+       |
    }                         // ----------+
}
```

`x`のライフタイムは`r`のライフタイムよりも大きい  
→`x`が有効な間は`r`も有効であることがわかる

---

## ライフタイム（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=57227f06149897d4d5813aca227ce73d)）

```rust
fn main() {
    {
        let r;                // ---------+-- 'a
                              //          |
        {                     //          |
            let x = 5;        // -+-- 'b  |
            r = &x;           //  |       |
        }                     // -+       |
                              //          |
        println!("r: {}", r); //          |
    }                         // ---------+
}
```

`x`のライフタイムは`r`のライフタイムよりも小さい  
→内側のスコープを抜けると`x`が解放されて無効になる
→`r`は無効なメモリを指すことになるのでコンパイルエラー

---

## ライフタイム（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=a729621c41b51af75b243ddc55798a4b)）

```rust
// 2つの引数のうち長い方の参照を返す関数
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

fn main() {
    let string1 = String::from("long string is long");

    let result;
    {
        let string2 = String::from("xyz");
        result = longest(string1.as_str(), string2.as_str());
        println!("The longest string is {}", result);   // OK
    }
    // resultにはstring2が入る可能性があるが、string2はこのスコープを抜けると解放されるため
    // resultは不正なメモリを参照してしまうかもしれない

    println!("The longest string is {}", result);       // NG
}
```

---

## ライフタイムがもたらすメリット

- ダングリングポインタを防止できる
  - 解放済み領域へのアクセス
  - 二重解放

※ダングリングポインタ: 解放済みなどで無効になったメモリ領域を指すポインタ

---

## 所有権とライフタイム

この2つがRustのメモリ安全性を支えている。

### 所有権

- 二重解放の防止
- メモリリークの防止

### ライフタイム

- ダングリングポインタの防止

---

# その他の特徴

---

## その他の特徴（その1）

- [変数の可変性](https://doc.rust-jp.rs/book-ja/ch03-01-variables-and-mutability.html)
  - immutable（不変）が基本
  - mutable（可変）にする場合は`let mut`のように`mut`をつける必要がある
- [ジェネリクス](https://doc.rust-jp.rs/book-ja/ch10-01-syntax.html)
  - 他の言語のジェネリクスとだいたい同じ
  - 例: `Option<T>`, `fn foo<T>(arg: T) { ... }`
- [パターンマッチ](https://doc.rust-jp.rs/book-ja/ch06-00-enums.html)
  - パターン漏れはコンパイルエラー
  - 値を保持する`enum`などから値を取り出して処理することもできる
  - ガードを書くこともできる  
    （`Some(x) if x < 10 => println!("10以下です")`）

---

## その他の特徴（その2）

- [モジュール、クレート](https://doc.rust-jp.rs/book-ja/ch07-02-defining-modules-to-control-scope-and-privacy.html)
  - モジュール: コードをグループ化するための概念（≒名前空間）
  - クレート: Rustにおけるコンパイル単位（バイナリ形式<small>（実行ファイル）</small>とライブラリ形式がある）
- [マクロ](https://doc.rust-jp.rs/book-ja/ch19-06-macros.html)
  - コンパイル時にコードの一部を書き換える機能
  - 宣言的マクロと手続き的マクロの2種類がある
- [アトリビュート（注釈）](https://doc.rust-jp.rs/rust-by-example-ja/attribute.html)
  - モジュールや要素に対するメタデータ
  - コンパイル時の様々な指示やマクロの使用、テスト関数の定義などで使う

---

# Rustのエラー処理

---

## [Rustのエラー処理](https://doc.rust-jp.rs/book-ja/ch09-00-error-handling.html)

- Rustには例外機構（`try-catch`とか`throw`とか）がない
- Rustではエラーは大きく2つに分類される
  - 回復可能なエラー → `Result<T, E>`型で扱う
  - 回復不可能なエラー → `panic!`で異常終了させる
- 基本的に`Result`型の戻り値を受け取ってエラー処理を行うことになる
  - 煩雑にならないように、エラーだったら即座に`panic!`したり、エラーの入った`Result`値をそのまま関数自身の戻り値として返す機能がある

---

## エラー処理の例

```rust
use std::fs::File;

fn main() {
    let f = File::open("hello.txt");

    let f = match f {
        Ok(file) => file,
        Err(error) => {
            // ファイルを開く際に問題がありました
            panic!("There was a problem opening the file: {:?}", error)
        },
    };
}
```

---

## エラー処理のショートカット

```rust
use std::fs::File;

fn main() {
    // unwrap()はエラーだった場合にpanic!を呼んでくれる
    let f = File::open("hello.txt").unwrap();
}
```

```rust
use std::io;
use std::io::Read;
use std::fs::File;

fn read_username_from_file() -> Result<String, io::Error> {
    // ?演算子はResultの中身がOkならその値を返し、Errならその場でErrをreturnする
    let mut f = File::open("hello.txt")?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}
```

---

# Rustのエコシステム

---

## Rustのエコシステム

- 標準ライブラリは小さい
  - 暗号化やJSONなどの今どきの必需品どころか、正規表現や乱数といった他言語では標準でありそうなものすら含まれていない
  - すべてクレート（パッケージ）で導入する方針
  - 参考: [Rustの標準ライブラリは小さいのか？ - Qiita](https://qiita.com/dalance/items/5816618a308e0058136b)
- [Cargo](https://doc.rust-lang.org/cargo/)というビルドツール兼パッケージマネージャが用意されている
  - [crates.io](https://crates.io/): パッケージレジストリ
- [rustup](https://rustup.rs/)というRustの環境構築ツールが用意されている
- ツール
  - テスト、ドキュメント、フォーマッタ、Lint、LSP、VS Code拡張
- 幅広い[対応プラットフォーム](https://doc.rust-lang.org/nightly/rustc/platform-support.html)

**現代的な開発に必要なものは最初からすべて揃っている**

---

## クロスコンパイル

- Rustは標準でクロスコンパイルをサポート
  - ただしGoほど簡単ではない
- ターゲット環境用のCツールチェイン（`gcc`とか）が必要
- 簡単にクロスコンパイルするための方法
  - Dockerを使う
  - [cross](https://github.com/cross-rs/cross)を使う<small>（たぶんこれが一番簡単かつメジャー）</small>
    - DockerやPodmanを使って簡単にクロスコンパイルするためのツール
  - [cargo-zigbuild](https://github.com/rust-cross/cargo-zigbuild)を使う
    - Zigのツールチェインを使ってクロスコンパイルする
    - 参考: [zig cc がクロスコンパイルに便利な理由](https://zenn.dev/tetsu_koba/articles/b54e954809a86c)
- 参考: [対応プラットフォーム](https://doc.rust-lang.org/nightly/rustc/platform-support.html)

---

### クロスコンパイルの例

1. ターゲット環境のツールチェインをインストール  
   <small>ここが一番たいへん（ターゲット環境のコンパイラが都合よくあればいいが…）</small>
   ```sh
   sudo apt install mingw-w64
   rustup target add x86_64-pc-windows-gnu
   ```
2. `.cargo/config`を作成してターゲット環境用の`linker`を設定
   ```toml
   [target.x86_64-pc-windows-gnu]
   linker = "/usr/bin/x86_64-w64-mingw32-gcc"
   ```
3. ビルド
   ```sh
   cargo build --target x86_64-pc-windows-gnu
   ```

---

### クロスコンパイルの例（`cross`を利用）

1. DockerかPodmanを使えるようにしておく
2. [cross](https://github.com/cross-rs/cross)をインストール
   ```sh
   cargo install cross
   ```
3. ビルド
   ```sh
   cross build --target x86_64-pc-windows-gnu
   ```

ホスト環境ではターゲット環境のツールチェインを用意する必要がない。
`cross`が**Dockerを使って勝手にやってくれる。便利！！**

---

### サポートするターゲット環境（2023年2月現在）

```console
> rustup target list | column -c 120 | expand
aarch64-apple-darwin                    i586-unknown-linux-gnu                  thumbv6m-none-eabi
aarch64-apple-ios                       i586-unknown-linux-musl                 thumbv7em-none-eabi
aarch64-apple-ios-sim                   i686-linux-android                      thumbv7em-none-eabihf
aarch64-fuchsia                         i686-pc-windows-gnu                     thumbv7m-none-eabi
aarch64-linux-android                   i686-pc-windows-msvc                    thumbv7neon-linux-androideabi
aarch64-pc-windows-msvc                 i686-unknown-freebsd                    thumbv7neon-unknown-linux-gnueabihf
aarch64-unknown-linux-gnu               i686-unknown-linux-gnu                  thumbv8m.base-none-eabi
aarch64-unknown-linux-musl              i686-unknown-linux-musl                 thumbv8m.main-none-eabi
aarch64-unknown-none                    i686-unknown-uefi                       thumbv8m.main-none-eabihf
aarch64-unknown-none-softfloat          mips-unknown-linux-gnu                  wasm32-unknown-emscripten
aarch64-unknown-uefi                    mips-unknown-linux-musl                 wasm32-unknown-unknown
arm-linux-androideabi                   mips64-unknown-linux-gnuabi64           wasm32-wasi
arm-unknown-linux-gnueabi               mips64-unknown-linux-muslabi64          x86_64-apple-darwin (installed)
arm-unknown-linux-gnueabihf             mips64el-unknown-linux-gnuabi64         x86_64-apple-ios
arm-unknown-linux-musleabi              mips64el-unknown-linux-muslabi64        x86_64-fortanix-unknown-sgx
arm-unknown-linux-musleabihf            mipsel-unknown-linux-gnu                x86_64-fuchsia
armebv7r-none-eabi                      mipsel-unknown-linux-musl               x86_64-linux-android
armebv7r-none-eabihf                    nvptx64-nvidia-cuda                     x86_64-pc-solaris
armv5te-unknown-linux-gnueabi           powerpc-unknown-linux-gnu               x86_64-pc-windows-gnu
armv5te-unknown-linux-musleabi          powerpc64-unknown-linux-gnu             x86_64-pc-windows-msvc
armv7-linux-androideabi                 powerpc64le-unknown-linux-gnu           x86_64-sun-solaris
armv7-unknown-linux-gnueabi             riscv32i-unknown-none-elf               x86_64-unknown-freebsd
armv7-unknown-linux-gnueabihf           riscv32imac-unknown-none-elf            x86_64-unknown-illumos
armv7-unknown-linux-musleabi            riscv32imc-unknown-none-elf             x86_64-unknown-linux-gnu
armv7-unknown-linux-musleabihf          riscv64gc-unknown-linux-gnu             x86_64-unknown-linux-gnux32
armv7a-none-eabi                        riscv64gc-unknown-none-elf              x86_64-unknown-linux-musl
armv7r-none-eabi                        riscv64imac-unknown-none-elf            x86_64-unknown-netbsd
armv7r-none-eabihf                      s390x-unknown-linux-gnu                 x86_64-unknown-none
asmjs-unknown-emscripten                sparc64-unknown-linux-gnu               x86_64-unknown-redox
i586-pc-windows-msvc                    sparcv9-sun-solaris                     x86_64-unknown-uefi
```

---

# 利用分野と有名なプロダクト

---

## どのような分野で使われているか

- CやC++の置き換え（部分的に置き換えたり、丸ごと刷新したり）
  - 競合: [Zig](https://ziglang.org/)、[Carbon](https://github.com/carbon-language/carbon-lang)
- システムプログラミング
  - OSカーネルやデバイスドライバ、ミドルウェア等
  - Linux、Android、Windows
- Webブラウザのエンジン（Firefox）
- フロントエンドのエコシステム
  - モジュールバンドラー、トランスパイラ等（[SWC](https://swc.rs/)、[Turbopack](https://turbo.build/pack)）
  - JavaScript/TypeScriptランタイム環境（[Deno](https://deno.land/)）
- WebAssembly
- 画像処理、動画処理
- ユーティリティ等のコマンドラインツール
  - 参考: [uutils/coreutils](https://github.com/uutils/coreutils)、[Rewritten in Rust](https://zaiste.net/posts/shell-commands-rust/)

---

## 主流ではない分野（あるいは、これから来るかも？）

- Webアプリケーション
  - フレームワークはいくつかある（参考: [Rust web framework comparison](https://github.com/flosse/rust-web-framework-comparison)）
- デスクトップアプリケーション
  - [Tauri](https://tauri.app/)（[Electron](https://www.electronjs.org/)の競合）
    - Web技術でデスクトップアプリを作るフレームワーク
- モバイルアプリケーション
- ゲーム
  - ゲームエンジンや画像処理など特定の部分で使われるかも？
  - <small>余談：「Rust」という名前のゲームがあるが全然関係ない（検索するときに邪魔）</small>
- 組み込み
  - 組み込みもターゲットにはしているが、既存分野は厳しそう
  - IoTとかの新しい機器とかはありそう

---

## Rust製の主なプロダクト

- [Firefox](https://www.mozilla.org/ja/firefox/): Webブラウザ
- [Deno](https://deno.land/): JavaScript/TypeScriptのランタイム環境
- [SWC](https://swc.rs/): JavaScript/TypeScriptトランスパイラ
- [Turbopack](https://turbo.build/pack): JavaScript/TypeScriptバンドルツール
- [Ruffle](https://ruffle.rs/): Adobe Flashエミュレーター
- [SurrealDB](https://surrealdb.com/): データベースシステム
- [ripgrep](https://github.com/BurntSushi/ripgrep): grepの代替
- [Alacritty](https://alacritty.org/): ターミナルエミュレーター
- [wezterm](https://wezfurlong.org/wezterm/): ターミナルエミュレーター

参考: [Awesome Rust](https://awesome-rust.com/)

---

## Rustの採用事例（プロダクト）

- Firefox: 2017年にエンジンをRust製に切り替え開始（[参考記事](https://www.publickey1.jp/blog/16/firefox_project_quantum.html)）
- Linux: カーネル 6.1からRust導入（[参考記事](https://codezine.jp/article/detail/17038)）
- Windows: 2019年からWindows 10の一部にRustの採用（[参考記事](https://news.mynavi.jp/techplus/article/20191205-933334/)）
- Android: 2021年からRustを採用（[参考記事](https://japan.zdnet.com/article/35196972/)）
- Chromium: 2023年Rustの採用を発表（[参考記事](https://developers-jp.googleblog.com/2023/02/supporting-use-of-rust-in-chromium.html)）

---

### Rustの採用事例（企業）

- Amazon: [AWSがプログラミング言語「Rust」に期待する理由](https://japan.zdnet.com/article/35183866/)
- Meta: [Meta、Rustの採用を加速--高性能バックエンドサービスで](https://japan.zdnet.com/article/35191156/)
- Oracle: [Rust言語による新しいDockerコンテナランタイム実装「Railcar」](https://www.publickey1.jp/blog/17/rustdockerrailcarrust.html)
- Cloudflare: [Cloudflare、NGINXに代えて自社開発のRust製HTTPプロキシ「Pingora」をグローバルCDNに採用](https://www.publickey1.jp/blog/22/cloudflarenginxrusthttppingoracdncpu31.html)
- Dropbox: [Dropbox、4年をかけてRust言語で再構築された新しい同期エンジン「Nucleus」をリリース](https://forest.watch.impress.co.jp/docs/news/1255034.html)

参考記事:
- [Rustとはどんな言語か？3つの特色や使われているサービスを『詳解Rustプログラミング』から紹介](https://codezine.jp/article/detail/15149)
- [MS、AWS、Googleも本格採用へ--プログラミング言語「Rust」の最新動向を振り返る](https://japan.zdnet.com/article/35193608/)

---

# Rustのはじめかた

---

## 公式サイト・公式ドキュメント

https://www.rust-lang.org（[日本語](https://www.rust-lang.org/ja/)）

- [Getting started](https://www.rust-lang.org/learn/get-started)（[日本語](https://www.rust-lang.org/ja/learn/get-started)）
- [インストール](https://www.rust-lang.org/ja/tools/install)
- [ドキュメント](https://www.rust-lang.org/ja/learn)（[日本語ドキュメントサイト](https://doc.rust-jp.rs/)）
  - [The Book](https://doc.rust-lang.org/book/)（[日本語](https://doc.rust-jp.rs/book-ja/)）
  - [Rust by Example](https://doc.rust-lang.org/stable/rust-by-example/)（[日本語](https://doc.rust-jp.rs/rust-by-example-ja/)）
- [Playground](https://play.rust-lang.org/)
- [crates.io](https://crates.io/)（パッケージレジストリ）

---

## Rust by Example

https://doc.rust-lang.org/stable/rust-by-example/

- Rustのコンセプトと標準ライブラリをサンプルコードで紹介
- ブラウザ上で実行可能なようになっている

ここで一通り触ってみて、Rustがどんな感じなのか掴むとよい。

---

## Playground

https://play.rust-lang.org/

- ブラウザ上で実行できる環境
- ちょっとしたコードを書いてみるのに最適
  - 本や記事上のサンプルコードを試してみる
  - 書き方の勉強
- コードの共有ができる
  - 他人に見せたり教えたり

※本スライドのサンプルコードにも利用。

---

## 環境の準備

- コマンドライン環境の用意
  - macOS: `Termina.app`、[iTerm2](https://iterm2.com/)
  - Windows: `コマンドプロンプト`、[Windows Terminal](https://apps.microsoft.com/store/detail/windows-terminal/9N0DX20HK701?hl=ja-jp&gl=JP)
  - Visual Studio Codeの中のターミナル
- エディタの用意
- Gitの用意
- Rustのインストール

---

## エディタ

- [Visual Studio Code (VS Code)](https://code.visualstudio.com/) + [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer) **👍オススメ**
- IntelliJ系エディタ（[IDEA](https://www.jetbrains.com/ja-jp/idea/)や[CLion](https://www.jetbrains.com/ja-jp/clion/)） + [Rustプラグイン](https://www.jetbrains.com/ja-jp/rust/)
- お好きなテキストエディタ
  - LSP(Language Server Protocol)に対応しているとなおよい
    - 入力補完や定義の参照など、IDEがよく持っている機能を切り出して標準化したもの
    - Rustは`rust-analyzer`というLSP実装を提供している

Javaみたいに専用のIDEというのは見かけない。

---

## インストール

- [rustup](https://rustup.rs/)のインストール
  - Rustのインストーラー兼Rustのバージョン管理ツール
- Linux: 公式にある手順（`curl`と`sh`を使ったインストール）
  ```sh
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
  ```
- macOS: [Homebrew](https://formulae.brew.sh)
- Windows: [winget](https://github.com/microsoft/winget-cli)、[Scoop](https://scoop.sh/)、[Chocolatey](https://chocolatey.org/)

Rustを直接インストールすることもできるが、あまりおすすめできない。
`rustup`を使って管理しましょう。

---

## Rustの作法とツール

- Rustのプロジェクトは[Cargo](https://doc.rust-lang.org/cargo/)というツールを使って管理する
- Cargo: Rustのビルドツール兼パッケージマネージャー
- Rustをインストールすると一緒にインストールされる

```sh
# プロジェクトの作成
cargo new hello_world

# テスト
cargo test

# ビルド
cargo build
```

---

## Cargoの主なコマンド

| コマンド      | 意味                               |
| ------------- | ---------------------------------- |
| `cargo new`   | 新規プロジェクトの作成             |
| `cargo add`   | クレートの追加 ※v1.62以降          |
| `cargo run`   | プロジェクトの実行                 |
| `cargo build` | プロジェクトのビルド               |
| `cargo test`  | プロジェクトのテスト               |
| `cargo doc`   | プロジェクトのドキュメントのビルド |

---

## プロジェクトの作り方

### バイナリターゲット（実行ファイル）の場合

```sh
cargo new hello_world

# あるいは --bin を明示的に指定
cargo new --bin hello_world
```

### ライブラリターゲットの場合

```sh
cargo new --lib greeter
```

---

## プロジェクト構成

### ディレクトリ構造

```
hello_world
├── Cargo.lock ← パッケージ依存関係のロックファイル
├── Cargo.toml ← プロジェクトの設定など
└── src        ← ソースコード
    └── main.rs
```

### Cargo.toml

```toml
[package]
name = "hello_world"
version = "0.1.0"
edition = "2021"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
rand = "0.8.5"
```

---

## 開発の進め方

- `cargo new`して新規プロジェクトを作成する
- コードを書く
- `cargo run`で実行してみる
- テストコードを書く
- `cargo test`でテストを実行する
- `cargo build`でビルドする
- `cargo build --release`でリリースビルドする

---

## コーディング規約

- [Rust Style Guide](https://github.com/rust-lang/fmt-rfcs/blob/master/guide/guide.md): 公式のコーディング規約
- コードフォーマッターの`rustfmt`のルールにもなっている

### ざっくり

- インデント: 4スペース
- 1行は100文字まで
- 命名規則
  - クレート、モジュール、関数、変数 → **snake_case**
  - 型、トレイト、Enumのバリアント → **UpperCamelCase**
  - 定数、static変数 → **SCREAMING_SNAKE_CASE**

---

## 便利なツール

https://doc.rust-jp.rs/book-ja/appendix-04-useful-development-tools.html

- `rustfmt`: 公式のコードフォーマッター
  - インストール: `rustup component add rustfmt`
  - 実行: `cargo fmt`
- `rustfix`: コンパイラに警告されるコードを自動修正するツール
  - インストール: 不要（Rustと一緒にインストールされる）
  - 実行: `cargo fix`
- `Clippy`: 静的解析ツール
  - インストール: `rustup component add clippy`
  - 実行: `cargo clippy`

---

# 参考情報

---

## 参考リンク

- [公式サイト](https://www.rust-lang.org/)（[日本語](https://www.rust-lang.org/ja/)）
  - [Getting started](https://www.rust-lang.org/learn/get-started)（[日本語](https://www.rust-lang.org/ja/learn/get-started)）
  - [ドキュメント](https://www.rust-lang.org/ja/learn)（[日本語ドキュメントサイト](https://doc.rust-jp.rs/)）
    - [The Book](https://doc.rust-lang.org/book/)（[日本語](https://doc.rust-jp.rs/book-ja/)）
    - [Rust by Example](https://doc.rust-lang.org/stable/rust-by-example/)（[日本語](https://doc.rust-jp.rs/rust-by-example-ja/)）
  - [Playground](https://play.rust-lang.org/)
- [crates.io](https://crates.io/)（パッケージレジストリ）
- [Awesome Rust](https://awesome-rust.com/)
- [Rustのリンク集 - Qiita](https://qiita.com/mosh/items/7e327dafbe53b72ad99d)

---

## 書籍紹介

### 入門

- [『実践Rustプログラミング入門』](https://www.shuwasystem.co.jp/book/9784798061702.html)
  - JavaやPythonなどの他言語を知っている人を対象にわかりやすく解説
- [『Rustプログラミング完全ガイド』](https://book.impress.co.jp/books/1121101129)
  - こちらも他言語との比較を交えた解説本
- [『手を動かして考えればよくわかる 高効率言語 Rust 書きかた・作りかた』](https://www.socym.co.jp/book/1351)
  - Pythonとの比較で解説してくれる本。わかりやすい解説が特徴

---

## 書籍紹介

### 中級・実践

- [『詳解Rustプログラミング』](https://www.shoeisha.co.jp/book/detail/9784798160221)
  - Rustの基礎とシステムプログラミングなどの低レイヤーの題材を扱う
- [『プログラミング言語Rust入門』](https://bookplus.nikkei.com/atcl/catalog/20/P96850/)
  - 後半でWeb APIやデータベースなどの実践的な内容を扱う
- [『実践Rust入門』](https://gihyo.jp/book/2019/978-4-297-10559-4)
  - パーサーの作成、FFI、Webアプリ、データベースといった実践的な内容を扱う

---

# おわり

おつかれさまでした😃
