# ボツ原稿

## Rustの主な道具

<div class="columns">
<div>

- **構造体**
- トレイト
- ジェネリクス
- 可視性
- アトリビュート

</div>

<div>

- モジュール、クレート、パッケージ
- マクロ
- 所有権
- ライフタイム

</div>

</div>

---

## 構造体

- 他の言語の構造体とだいたい同じ
- 複数のデータをまとめた型
- `struct`キーワードで定義
- `impl`を使って構造体に対してメソッドや関連関数を定義できる
  - メソッド: 構造体のインスタンスを対象とした関数
  - 関連関数: インスタンスは取らない（コンストラクタなどに使われる）

---

## 構造体（[サンプルコード](https://doc.rust-jp.rs/book-ja/ch05-03-method-syntax.html)）

```rust
#[derive(Debug)]
struct Rectangle {  // 構造体の定義
    width: u32,
    height: u32,
}

impl Rectangle {    // Rectangle構造体のメソッドを実装する
    fn area(&self) -> u32 {
        self.width * self.height
    }
}

fn main() {
    let rect1 = Rectangle { width: 30, height: 50 };

    println!("The area of the rectangle is {} square pixels.", rect1.area());
}
```

---

## Rustの主な道具

<div class="columns">
<div>

- 構造体
- **トレイト**
- ジェネリクス
- 可視性
- アトリビュート

</div>

<div>

- モジュール、クレート、パッケージ
- マクロ
- 所有権
- ライフタイム

</div>

</div>

---

## トレイト

- 他の言語でいうインターフェイスのようなもの
- 共通の振る舞いを定義するためのもの
- `trait`キーワードで定義
- `impl トレイト名 for データ型`でトレイトを型に実装する
- トレイトにデフォルト実装を持たせることもできる

---

## トレイト（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=10d7c1639692d5205b0e99c7aa0a4619)）

```rust
trait Shape {               // トレイトの定義
    fn area(&self) -> f64;
}

struct Rectangle {          // 構造体の定義
    width: f64,
    height: f64,
}

impl Shape for Rectangle {  // RectangleにShapeトレイトを実装
    fn area(&self) -> f64 {
        self.width * self.height
    }
}

struct Circle {             // 別の構造体
    radius: f64,
}

impl Shape for Circle {     // CircleにもShapeトレイトを実装
    fn area(&self) -> f64 {
        3.14 *  self.radius * self.radius
    }
}

fn print_area(name: &str, shape: &impl Shape) {  // Shapeトレイトを実装した型を受け取る
    println!("{}の面積: {}", name, shape.area())  // Shapeトレイトのarea()を呼び出す
}
```

---

## Rustの主な道具

<div class="columns">
<div>

- 構造体
- トレイト
- **ジェネリクス**
- 可視性
- アトリビュート

</div>

<div>

- モジュール、クレート、パッケージ
- マクロ
- 所有権
- ライフタイム

</div>

</div>

---

## ジェネリクス

- 他の言語のジェネリクスとだいたい同じ
- 型と関数を汎用的に使えるようにするためのもの
- 型パラメータを`<T>`のような形で記述する
  - 例: `Option<T>`, `fn foo<T>(arg: T) { ... }`
  - トレイト境界（ジェネリック境界）の例:  
    `fn printer<T: Display>(arg: T) { ... }`  
    （`T`は`Display`を実装している必要がある）

---

## ジェネリクス（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=56762a1c7243a0a45e31e70eadb1e87f)）

```rust
#[allow(dead_code)]
struct Point<T> {   // T型を取る構造体の定義
    x: T,           // T型のフィールドx
    y: T,           // T型のフィールドy
}

impl<T> Point<T> {  // Point<T>の実装
    fn x(&self) -> &T {
        &self.x
    }
}

// トレイト境界を指定した関数
// Tはstd::fmt::Displayを実装した型でなければいけない
fn print_value<T: std::fmt::Display>(value: T) {
    println!("value is {}", value);
}

fn main() {
    let p = Point { x: 5, y: 10 };

    println!("p.x = {}", p.x());
    print_value(p.x());
}
```

---

## Rustの主な道具

<div class="columns">
<div>

- 構造体
- トレイト
- ジェネリクス
- **可視性**
- アトリビュート

</div>

<div>

- モジュール、クレート、パッケージ
- マクロ
- 所有権
- ライフタイム

</div>

</div>

---

## 可視性

- カプセル化のためのアクセス制御
  - 変数や関数がモジュールの外部から見えるかどうか
- **モジュール単位**で可視性が制御される（フィールド単位ではない）
  - （Javaのパッケージプライベートみたいな感じ？）
- パブリックとプライベートの2種類のみ
- デフォルトはプライベートで、パブリックにしたいときは`pub`をつける

---

## 可視性（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=22a44cbf1ac33609696e1e21a54ef48b)）

```rust
#[allow(dead_code)]
mod my_mod {
    // パブリックな構造体
    pub struct MyObject {
        pub id: i32,  // パブリックなフィールド
        data: i32,    // プライベートなフィールド
    }

    // パブリックな関数
    pub fn create_object(id: i32, data: i32) -> MyObject {
        return MyObject { id, data };
    }

    // プライベートな関数
    // 可視性はモジュール単位なので、プライベートなdataもアクセスできる
    fn private_function(obj: MyObject) {
        println!("id={}, data={}", obj.id, obj.data);
    }
}
```

---

## Rustの主な道具

<div class="columns">
<div>

- 構造体
- トレイト
- ジェネリクス
- 可視性
- **アトリビュート**

</div>

<div>

- モジュール、クレート、パッケージ
- マクロ
- 所有権
- ライフタイム

</div>

</div>

---

## アトリビュート（注釈）

- モジュールや要素に対するメタデータ
  - 他の言語でクラスや関数の上に`@FooBar`みたいに書くやつと同じ
- 以下のような指示のために用いられる
  - コンパイル時の条件分岐
  - lintの無効化
  - マクロなどの使用
  - ユニットテストやベンチマーク用関数の定義
- `#[attribute(value)]`のように記述する（他にもバリエーションあり）

参考: [Rustでよく見かけるattributeを整理する。 - Qiita](https://qiita.com/8pockets/items/0b6bb582a1bfa995559e)

---

## アトリビュート（注釈）

```rust
#[allow(dead_code)]     // lintの制御

#[derive(Debug)]        // Debugトレイトの自動実装
struct Rectangle {
    width: u32,
    height: u32,
}

#[test]                 // テスト関数
fn test_rectangle() {
    ...
}
```

---

## Rustの主な道具

<div class="columns">
<div>

- 構造体
- トレイト
- ジェネリクス
- 可視性
- アトリビュート

</div>

<div>

- **モジュール、クレート、パッケージ**
- マクロ
- 所有権
- ライフタイム

</div>

</div>

---

## モジュール、クレート、パッケージ

- モジュール
  - ある機能単位などでコードをグループ化するための概念（≒名前空間）
  - `mod`キーワードで定義する
  - `use`キーワードでスコープに持ち込める
    - `use`せずに完全修飾でアクセスすることも可能
- クレート
  - Rustにおけるコンパイル単位
  - バイナリ形式（実行ファイル）とライブラリ形式がある
- パッケージ
  - パッケージマネージャー(Cargo)で管理される1つ以上のクレートの集合

---

## モジュール

```rust
// モジュールは入れ子にできる
pub mod foo {
    pub mod bar {
        pub fn do_something() {
            println!("do something!")
        }
    }
}

// useでモジュールパスをスコープに取り込む
use std::collections::HashMap;

#[allow(unused)]
fn main() {
    let mut map: HashMap<String, i32> = HashMap::new();
    foo::bar::do_something();
}
```

※一般的にはモジュールごとにファイル・ディレクトリを分ける。
※例のように1ファイルに複数のモジュールを定義することも可能。

---

## Rustの主な道具

<div class="columns">
<div>

- 構造体
- トレイト
- ジェネリクス
- 可視性
- アトリビュート

</div>

<div>

- モジュール、クレート、パッケージ
- **マクロ**
- 所有権
- ライフタイム

</div>

</div>

---

## マクロ

- コンパイル時にコードの一部を書き換える機能（メタプログラミングの一種）
- C言語の単純な文字列置換とは異なり、構文処理されるので危険性が低い
- 宣言的マクロと手続き的マクロの2種類がある
  - 宣言的マクロ: [`macro_rules!`](https://doc.rust-jp.rs/rust-by-example-ja/macros.html)を使って専用のDSLで書ける（比較的簡単）
  - 手続き的マクロ: マクロ専用のクレートを作って、ソースコードを変換する処理をRustで書く（難しいが自由度は高い）
    - 手続き的マクロは更に関数風マクロ、属性風マクロ、カスタムなderiveマクロの3種類がある
- 関数風マクロは名前の後に`!`をつけて使用する（`println!`とか`vec!`とか）

---

## マクロ（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=636ea6205b96ee64e5f0732564725f2b)）

```rust
macro_rules! add {
    ($a:expr, $b:expr) => {
        $a + $b
    };
}

fn main() {
    let a = add!(1, 2);
    println!("a = {}", a);
}
```

---

## Rustの主な道具

<div class="columns">
<div>

- 構造体
- トレイト
- ジェネリクス
- 可視性
- アトリビュート

</div>

<div>

- モジュール、クレート、パッケージ
- マクロ
- **所有権**
- ライフタイム

</div>

</div>

---

## 所有権

所有権はRustの特徴としてよく挙げられるもののひとつ。

- ガベージコレクションを使わずに安全にメモリを管理するための仕組み
- 所有権: 変数が値を保持できる権利（あるいは破棄できる権利）
- 所有者: 所有権を持った変数
  - いかなる時も所有者は1つ
  - 所有者（変数）がスコープから外れると値は破棄される
- 別の変数に代入したり、関数に渡したりすると所有権が移動（ムーブ）する
- 所有権を渡さずに参照させる**借用**という機能もある

---

## 所有権（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=6c59e2fff429a9d82eee098fa5a019fe)、[関数の例](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=d3b46113e3a4900177efea2df1ebf4ab)）

```rust
#[allow(unused_variables)]
fn main() {
    let s1 = String::from("Hello"); // String型の値をs1に束縛
    let s2 = s1;                    // s1からs2へ所有権が移動

    println!("{}", s1);             // s1は所有権を失っているので使えない！
} // ここでスコープを抜けて、s2のメモリを解放する（s1は所有権を失っているので何もしない）
```

```
error[E0382]: borrow of moved value: `s1`
 --> src/main.rs:6:20
  |
3 |     let s1 = String::from("Hello");
  |         -- move occurs because `s1` has type `String`, which does not implement the `Copy` trait
4 |     let s2 = s1;
  |              -- value moved here
5 |
6 |     println!("{}", s1);
  |                    ^^ value borrowed here after move
  |
help: consider cloning the value if the performance cost is acceptable
  |
4 |     let s2 = s1.clone();
  |                ++++++++
```

---

## 参照と借用（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=c1253224b1a91dd016df3bdc9e56bf1e)）

```rust
fn print_value(value: &String) { // 引数に参照を取る（借用と呼ぶ）
    println!("{}", value);
} // valueは借用であり、所有権を持っているわけではないので何もしない

fn main() {
    let s = String::from("Hello");  // sがスコープに入る

    print_value(&s);    // sの参照を渡す（所有権は渡さない）

    println!("{}", s);  // sの所有権を持っているので使える
}
```

- `参照(&)`は所有権を持たない
- 関数の引数に参照を取ることを`借用`と呼ぶ

---

## 所有権（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=9115098325bdf5d9f027be58935f4c6a)）

ところで、次のコードは前述の例を整数に変えただけだが正常に動く。

```rust
#[allow(unused_variables)]
fn main() {
    let a = 100;
    let b = a;    // ここでコピーされる（aは所有権を失わない）

    println!("{}", a);
}
```

- `i32`のように`Copy`トレイトが実装されている型はコピーされる（**コピーセマンティクス**になる）
  - それ以外の場合は所有権が移動する（**ムーブセマンティクス**）

---

## 所有権って難しくないですか！？

- 難しいです
- なんかわかんないけどコンパイルが通らなくて躓く人多数

---

## 所有権がもたらすメリット

- メモリの二重開放を防止できる
  - 所有者がメモリを解放する
  - 所有権を持っていないものは何もしない
- メモリリークを防止できる
  - スコープを抜ける時にメモリを解放する
- GCのような実行時のメモリ管理コストが必要ない

---

## Rustの主な道具

<div class="columns">
<div>

- 構造体
- トレイト
- ジェネリクス
- 可視性
- アトリビュート

</div>

<div>

- モジュール、クレート、パッケージ
- マクロ
- 所有権
- **ライフタイム**

</div>

</div>

---

## ライフタイム

所有権と並び、Rustの特徴としてよく挙げられるもののひとつ。

- 参照が有効な期間のこと
- ライフタイムによって借用に問題がないことをチェックする
- ライフタイム注釈
  - `'a`のようにアポストロフィーに続く名前で表現する
    例: `&'a i32`
- 大体の場合、ライフタイムは暗黙的に推論される（わざわざ注釈を書かなくてもよい）
- プログラムの実行中に常に存在していることを示す`'static`もある（文字列リテラルなど）

---

## ライフタイム（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=90e61012a77096721e25edb6e58b6261)）

```rust
fn main() {
    {
        let x = 5;            // ----------+-- 'b
                              //           |
        let r = &x;           // --+-- 'a  |
                              //   |       |
        println!("r: {}", r); //   |       |
                              // --+       |
    }                         // ----------+
}
```

`x`のライフタイムは`r`のライフタイムよりも大きい  
→`x`が有効な間は`r`も有効であることがわかる

---

## ライフタイム（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=57227f06149897d4d5813aca227ce73d)）

```rust
fn main() {
    {
        let r;                // ---------+-- 'a
                              //          |
        {                     //          |
            let x = 5;        // -+-- 'b  |
            r = &x;           //  |       |
        }                     // -+       |
                              //          |
        println!("r: {}", r); //          |
    }                         // ---------+
}
```

`x`のライフタイムは`r`のライフタイムよりも小さい  
→内側のスコープを抜けると`x`が解放されて無効になる
→`r`は無効なメモリを指すことになるのでコンパイルエラー

---

## ライフタイム（[サンプルコード](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=a729621c41b51af75b243ddc55798a4b)）

```rust
// 2つの引数のうち長い方の参照を返す関数
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

fn main() {
    let string1 = String::from("long string is long");

    let result;
    {
        let string2 = String::from("xyz");
        result = longest(string1.as_str(), string2.as_str());
        println!("The longest string is {}", result);   // OK
    }
    // resultにはstring2が入る可能性があるが、string2はこのスコープを抜けると解放されるため
    // resultは不正なメモリを参照してしまうかもしれない

    println!("The longest string is {}", result);       // NG
}
```

---

## ライフタイムがもたらすメリット

- ダングリングポインタを防止できる
  - ダングリングポインタ: 解放済みなどで無効になったメモリ領域を指すポインタ
  - 解放済み領域へのアクセス
  - 二重解放
