const path = require("path");
const qrcode = require("qrcode");

const projectdir = (...paths) => path.join(__dirname, "../", ...paths);
const url = process.env.npm_package_homepage;
if (url === undefined) {
  console.error("homepage not defined in package.json");
  process.exit(1);
}

qrcode.toFile(projectdir("slides/images/qrcode.png"), url);
